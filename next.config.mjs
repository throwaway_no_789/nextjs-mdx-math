import with_mdx from '@next/mdx';
import remark_math from 'remark-math';
import rehype_mathjax from 'rehype-mathjax';

const withMDX = with_mdx({
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [remark_math],
    rehypePlugins: [rehype_mathjax],
  },
})

export default withMDX({
  pageExtensions: ['js', 'jsx', 'mdx'],
});
